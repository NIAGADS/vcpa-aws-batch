FROM ubuntu:18.04
MAINTAINER Otto Valladares <ottov.upenn@gmail.com>

RUN apt-get -q update && \
     apt-get --autoremove install --no-install-recommends -y -qq \
        openjdk-8-jre-headless curl python-minimal python-httplib2 pigz time git openssh-client parallel libgomp1 jq nano fuse && \
     rm -fR /var/cache/apt /var/lib/apt/lists && mkdir -p /root/.parallel && touch /root/.parallel/will-cite

ENV NGS /mnt/data/NGS
COPY id_rsa /root/.ssh/id_rsa
COPY picard.jar $NGS/jar/picard-git/build/libs/picard.jar
COPY GenomeAnalysisTK-3.7.jar $NGS/jar/gatk-3.7/GenomeAnalysisTK.jar
COPY libIntelDeflater.so $NGS/jar/picard-git/htsjdk/lib/jni/libIntelDeflater.so
COPY sam* $NGS/bin/
COPY bam $NGS/bin/
COPY tabix bgzip bcftools /usr/local/bin/
COPY bwa $NGS/bin/bwa-git/bwa
COPY xatlas $NGS/bin/

RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python && \
     pip install --no-cache-dir -U pysam boto3

# AWS CLI; wget "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip"
COPY awscli-bundle /awscli-bundle
RUN /awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws && \
      rm -fR /awscli-bundle
# GATK 4 - requires libgomp1
COPY gatk-4.1.1.0 /gatk-4.1.1.0
#COPY gatk-4.1.0.0 /gatk4.1

# get goofys
RUN curl -sS -L --output /usr/local/bin/goofys "https://github.com/kahing/goofys/releases/download/v0.24.0/goofys"

# git key
RUN chmod 600 /root/.ssh/id_rsa

ENV DIR /repos/adsp-wgs-pipeline
ENV GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

RUN git clone --single-branch --branch docker git@bitbucket.org:ottov123/adsp-wgs-pipeline.git $DIR/

